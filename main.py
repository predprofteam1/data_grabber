TOKEN = str(open("token", "r").read()).rstrip("\n")

import requests
import sqlite3
import json
import time

db = sqlite3.connect("data.db", isolation_level=None)
cursor = db.cursor()

cursor.execute("CREATE TABLE IF NOT EXISTS city_temp (id INTEGER PRIMARY KEY AUTOINCREMENT, city_id INTEGER, city_name TEXT, time INTEGER, temperature INTEGER);")
cursor.execute("CREATE TABLE IF NOT EXISTS room_temp (id INTEGER PRIMARY KEY AUTOINCREMENT, city_id INTEGER, area_id INTEGER, house_id INTEGER, room_id INTEGER, time INTEGER, temperature INTEGER);")

db.commit()

def api_req(path):
	f_path = "https://dt.miet.ru/ppo_it/api/" + str(path)
	return requests.get(f_path, headers={"X-Auth-Token": TOKEN}).json()

# api_req("1/temperature")

city_amount = 16
min_area_amount = 5
house_amount = 5
room_amount = 10

area_amount = min_area_amount
current_city = 1
current_area = 1
current_house = 1
current_room = 1

get_city_data = True

while True:
	if current_room > room_amount: # move to next house
		print("moving to next house")
		current_house += 1
		current_room = 1

	if current_house > house_amount: # move to next area
		print("moving to next area")
		current_area += 1
		current_house = current_room = 1

	if current_area > area_amount: # move to next city
		print("moving to next city")
		current_city += 1
		current_area = current_room = current_house = 1
		get_city_data = True

	if current_city > city_amount: # reset
		print("reset")
		current_city = current_area = current_room = 1
		get_city_data = True

	if get_city_data:
		city_data = api_req(current_city)
		area_amount = min(city_data["data"]["area_count"], min_area_amount)
		city_name = city_data["data"]["city_name"]
		city_temperature = city_data["data"]["temperature"]

		cursor.execute("INSERT INTO city_temp(city_id, city_name, time, temperature) VALUES(?, ?, ?, ?)", 
						(current_city, city_name, time.time(), city_temperature))

		print(f"city {current_city} - {city_name}")
		get_city_data = False
	else:
		room_data = api_req(f"{current_city}/{current_area}/{current_house}/{current_room}")
		room_temperature = room_data["data"]["temperature"]

		cursor.execute("INSERT INTO room_temp(city_id, area_id, house_id, room_id, time, temperature) VALUES(?, ?, ?, ?, ?, ?)",
						(current_city, current_area, current_house, current_room, time.time(), room_temperature))

		print(f"{current_city} - {current_area} - {current_house} - {current_room} - {room_temperature}")
		current_room += 1

	time.sleep(15)
